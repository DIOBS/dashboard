# Modelo de Dashboard em R

Modelo de _dashboard_ do [Observatório de Fortaleza](https://observatoriodefortaleza.fortaleza.ce.gov.br/).

[![Dashboard](www/screenshot.png)](https://diobs.shinyapps.io/dashboard/)
